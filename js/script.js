document.querySelector("#login_btn").setAttribute("onclick", "validacion()");
let user = document.querySelector("#username");
let pass = document.querySelector("#password");
let mens = document.querySelector("#mensaje");
user.focus();

fetch("usuarios-registrados.csv")
    .then(function (res) {
        return res.text();
    })
    .then(function (data) {
        cargaArray(data);
    });

let usuarios_registrados = [];

function cargaArray(data) {
    let filas = data.split(/\r?\n|\r/);
    for (let i = 0; i < filas.length; i++) {
        let celdasFila = filas[i].split(",");
        usuarios_registrados[i] = { nombre: celdasFila[0], password: celdasFila[1] };
        console.log(usuarios_registrados[i]);
    }
}
//------DOS VARIABLES PARA LA VERIFICACION CON UN SOLO USUARIO---------
//const nombre = "Eugenia";
//const password = "1234";

//------ARRAY DE OBJETOS PARA LA VERIFICACION CON VARIOS USUARIOS REGISTRADOS-------
// let usuarios_registrados = [];
// usuarios_registrados[0] = {nombre: "Camila", password: "12345678"},
// usuarios_registrados[1] = {nombre: "Ariel", password: "87654321"};
//------RECORRER LOS USUARIOS REGISTRADOS----------

function validacion() {
    mens.className = "rojo";
    //---------VERIFICACION QUE NO ESTÉN LOS INPUT VACÍOS----------
    if (user.value.trim().length === 0) {
        //dummy
        mens.value = "FALTA NOMBRE DE USUARIO";
        user.value = "";
        user.focus();
        return;
    }
    user.value = user.value.trim();

    if (pass.value.trim() === "") {
        //dummy
        mens.value = "FALTA PASSWORD";
        pass.value = "";
        pass.focus();
        return;
    }

    //---------VERIFICACION CON UN USUARIO---------------
    // if (nombre !== user.value || password !== pass.value) {
    //     mens.value = "USUARIO INEXISTENTE";
    //     user.focus();
    //     user.value = "";
    //     pass.value = "";
    //     return;
    // }

    //---------VERIFICACION CON VARIOS USUARIOS----------------
    let encontroUser;
    encontroUser = false;
    for (let i = 0; i < usuarios_registrados.length; i++) {
        // || representan O en una condición mientras que && representan Y en una condición
        console.log("Estamos recorriendo el elemento ", i, ", es decir, el usuario registrado: " + usuarios_registrados[i].nombre);
        if (usuarios_registrados[i].nombre === user.value && usuarios_registrados[i].password === pass.value) {
            encontroUser = true;
            console.log("encontrado");
            break;
        }
    }
    if (!encontroUser) {
        mens.value = "USUARIO INEXISTENTE";
        user.focus();
        user.value = "";
        pass.value = "";
        return;
    }


    mens.className = "verde";
    mens.value = "BIENVENIDO/A " + user.value.toUpperCase() + "...";

}

//fuente: http://www.forosdelweb.com/f13/hacer-que-tecla-enter-funcione-como-tab-365802/
function KeyAscii(e) {
    return (document) ? e.keyCode : e.which;
}

function TabKey(e, nextobject) {
    nextobject = document.getElementById(nextobject);
    if (nextobject) {
        if (KeyAscii(e) == 13) nextobject.focus();
    }
}
